import { CommonModule } from "@angular/common";
import { ChangeDetectionStrategy, Component, type OnInit } from "@angular/core";
interface EventItem {
  companyName?: string;
  date?: string;
  description?: string;
  positionInTheCompany?: string;
  image?: string;
}
@Component({
  selector: "app-timeline",
  templateUrl: "./timeline.component.html",
  styleUrls: ["./timeline.component.scss"],
})
export class TimelineComponent implements OnInit {
  name = "";
  events: EventItem[];

  constructor() {
    this.events = [
      {
        companyName: "TCS",
        date: "Apr 2023—Oct 2023",
        positionInTheCompany: "Front End Senior Developer",
        description: "At TCS, I worked on a project for a Nuclear Power Plant in the USA, updating the control dashboard. I created new components using the client's design system and developed reusable visual elements. I implemented input fields with integrated ngModel for seamless form integration and validation, ensuring data was saved and logged for security whenever users switched fields. Additionally, I integrated the dashboard login system with the user’s access badge using Capacitor to read magnetic cards through a custom package. The project, completed with Angular 16, upgraded the dashboard from Angular 12, enhancing its functionality and security.",
      },
      {
        companyName: "Catch Media",
        date: "Feb 2023—Nov 2023",
        positionInTheCompany: "Angular Developer",
        description: "At Catch Media, I initially started as a freelance Angular Developer, and due to my contributions, I was later offered a half-time position. I worked on a betting system that integrates with streaming sites/apps. Initially developed with AngularJS and later updated to Angular 16, I transitioned it to an npm package and combined it with Stencil for better integration. I migrated key features from AngularJS, developed AWS functions for user registration and flash bets, and created an admin dashboard with PrimeNG. I designed new login screens, implemented payment methods for credit cards and cryptocurrencies, optimized modules for lazy loading, and conducted QA tests using Jasmine. I also implemented a dynamic app identification system based on an AWS config file, ensuring the app remained up-to-date without requiring clients to reinstall the solution.",
      },
      {
        companyName: "BackBase",
        date: "Apr, 2022—Dec,2022",
        positionInTheCompany: "Front-End Eng",
        description: "As a Front-End Engineer at BackBase, I contributed to a pivotal project aimed at modernizing the system for WSECU, a prominent American credit union. I led the implementation of crucial features using Angular 14, including a redesigned login page, an improved credit card interface, and interactive modals for streamlined credit card management. I also played a key role in upgrading the system from Angular 8 to Angular 14, integrating the latest advancements and ensuring compatibility with cutting-edge technologies. Additionally, I led the initiative to transition and implement a new theme for the user dashboard, ensuring a cohesive and visually appealing interface aligned with modern design standards. I advocated for enhanced reactive programming by integrating additional RxJS functionalities to optimize code performance and responsiveness within the application.",
      },
      {
        companyName: "ASX Sports",
        date: "Aug, 2021—Apr, 2022",
        positionInTheCompany: "Front End Developer",
        description: "User worked as a Front End Developer at ASX Sports, collaborating on an Angular-based dashboard for sports betting leveraging blockchain technology. Led the design and implementation of functionalities for creating and managing unique bets with blockchain tokens for authenticity. Orchestrated the dashboard's capabilities to administer betting opportunities across various sports matches and generate analytics pages. Enhanced user experience with diverse betting options, including predictions and complex bets involving match variables.",
      },
    ];
  }
  ngOnInit(): void {}
}
