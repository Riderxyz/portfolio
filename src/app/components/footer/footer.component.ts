import { CommonModule } from '@angular/common';
import {  Component, inject, OnInit } from '@angular/core';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
visitanteObj ={
  visitanteName: '',
  visitanteEmail: '',
  visitanteMessage: ''
}
private toastSrv = inject(ToastService);
  ngOnInit(): void { }



  sendEmail() {
    this.toastSrv.notify(
      'success',
      'The email was sent',
      `I'll look up your message ASAP. Thanks`,
      5000
    );
  }
}
