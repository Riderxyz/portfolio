import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";
import { TimelineComponent } from "./timeline/timeline.component";
import { AboutComponent } from "./about/about.component";
import { HomeComponent } from "./home/home.component";
import { ToastComponent } from "./toast/toast.component";


import { ToolbarModule } from "primeng/toolbar";
import { ButtonModule } from "primeng/button";
import { CardModule } from "primeng/card";
import { TimelineModule } from "primeng/timeline";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { TooltipModule } from "primeng/tooltip";
import { MessagesModule } from "primeng/messages";
import { MessageModule } from "primeng/message";
import { ToastModule } from "primeng/toast";


const primeModules = [
  ToolbarModule,
  ButtonModule,
  CardModule,
  TimelineModule,
  InputTextModule,
  InputTextareaModule,
  TooltipModule,
  MessagesModule,
  MessageModule,
  ToastModule,
];

@NgModule({
  imports: [...primeModules, CommonModule, FormsModule],
  declarations: [
    HeaderComponent,
    FooterComponent,
    TimelineComponent,
    AboutComponent,
    HomeComponent,
    ToastComponent,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    TimelineComponent,
    AboutComponent,
    HomeComponent,
    ToastComponent
  ],

  providers: [],
})
export class ComponentModule {}
