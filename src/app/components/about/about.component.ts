import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  imagesArr = [
    {
      name: 'Angular',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fangular.png?alt=media&token=fc0f1434-b326-4887-961e-cdeac72528e9'
    },
    {
      name: 'Ionic',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fionic.png?alt=media&token=0c48a93f-4f23-4490-b493-6190f670224c'
    },
    {
      name: 'Firebase',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Ffirebase.png?alt=media&token=dc2c2fbd-8cf0-4b9b-b45f-9d693be3bf1f'
    },
    {
      name: 'Flutter',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fflutter.png?alt=media&token=10403484-3ea4-429f-8c97-15eabb9b0dd3'
    },
    {
      name: 'HTML 5',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fhtml5.png?alt=media&token=69b848f5-fc21-4a15-9b23-86b558a4204d'
    },
    {
      name: 'Typescript',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Ftypescript.png?alt=media&token=ff82b2aa-3859-4087-b159-ed41243c0ad3'
    },

    {
      name: 'Scss',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fscss.png?alt=media&token=f18e0ce9-17d2-4e8f-8b19-e9ae83210adb'
    },
    {
      name: 'NodeJs',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2FnodeJS.png?alt=media&token=946e9d82-0399-42b4-9952-f697c4c2fd98'
    },
    {
      name: 'RxJS',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Frxjs.png?alt=media&token=9f7c9d3f-7488-4f19-9883-8ef47e96b337'
    },
    {
      name: 'Visual Studio Code',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fvscode.png?alt=media&token=88355aad-8f3d-4157-8693-e18009242a32'
    },
  ]
  constructor() {

  }
}
