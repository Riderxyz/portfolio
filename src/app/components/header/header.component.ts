import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, type OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  buttonArr = [
    {
      name: 'Home',
      anchor: 'home',
    },
    {
      name: 'About',
      anchor: 'home',
    },
    {
      name: 'Skills',
      anchor: 'home',
    },
    {
      name: 'Projects',
      anchor: 'home',
    },
  ];
  ngOnInit(): void {}
}
