import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  imagesArr = [
    {
      name: 'Angular',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fangular.png?alt=media&token=fc0f1434-b326-4887-961e-cdeac72528e9'
    },
    {
      name: 'Ionic',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Fionic.png?alt=media&token=0c48a93f-4f23-4490-b493-6190f670224c'
    },
    {
      name: 'Firebase',
      path: 'https://firebasestorage.googleapis.com/v0/b/portfolio-29a8a.appspot.com/o/techStacks%2Ffirebase.png?alt=media&token=dc2c2fbd-8cf0-4b9b-b45f-9d693be3bf1f'
    },
  ]
  constructor() {

  }
}
